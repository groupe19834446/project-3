const email = document.getElementById("email");
const password = document.getElementById("password");
const form = document.getElementById("form");

form.addEventListener("submit", (e) => {
	e.preventDefault();
	validation();
});

function validation() {
	const emailValue = email.value;
	const passwordValue = password.value;
	let allInputsValid = true;

	if (emailValue === "") { 
		AfficherAlert(email, "Email should not be empty");
		allInputsValid = false;
	} else if(!emailValue.match(/^[a-zA-Z0-9\.\-\_]+@[a-zA-Z0-9\-\_]+\.[a-z]{2,}$/)) {
		AfficherAlert(email, "Email not correct");
		allInputsValid = false;
	} else {
		AfficherSucces(email);
	}

	if (passwordValue === ""){
		AfficherAlert(password, "Password should not be empty");
		allInputsValid = false;
	} else if (!passwordValue.match(/^[a-zA-Z0-9]{6,}$/)){
		AfficherAlert(password, "Password too short. Minimum 6 characters");
		allInputsValid = false;
	} else { 
		AfficherSucces(password);
	}
	if (allInputsValid) {
		form.submit();
	} else {
		form.removeEventListener("submit", function (e) {
			e.preventDefault();
			validation();
		});
	}
}

function AfficherAlert(input, message) {
	input.parentElement.classList.add("error");
	input.parentElement.classList.remove("success");
	input.nextElementSibling.textContent = message;
}

function AfficherSucces(input) {
	input.parentElement.classList.add("success");
	input.parentElement.classList.remove("error");
}
